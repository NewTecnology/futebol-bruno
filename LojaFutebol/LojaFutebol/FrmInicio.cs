﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LojaFutebol
{
    public partial class FrmInicio : Form
    {
        public static FrmInicio Atual;
        public FrmInicio()
        {
            InitializeComponent();
            Atual = this;
        }
        public void AbrirTela(UserControl tela)
        {
            if (panel5.Controls.Count > 1)
                panel5.Controls.RemoveAt(1);

            panel5.Controls.Add(tela);
        }
       
       
         private void toolStripMenuItem16_Click(object sender, EventArgs e)
        {
            Telas.Produto.frmCadastrarProduto tela = new Telas.Produto.frmCadastrarProduto();
            AbrirTela(tela);
        }

        private void toolStripMenuItem17_Click(object sender, EventArgs e)
        {
            Telas.Produto.frmConsultarProduto tela = new Telas.Produto.frmConsultarProduto();
            AbrirTela(tela);
        }

        private void toolStripMenuItem19_Click(object sender, EventArgs e)
        {
            Telas.Pedido.FrmCadastrarPedido tela = new Telas.Pedido.FrmCadastrarPedido();
            AbrirTela(tela);
        }

        private void toolStripMenuItem20_Click(object sender, EventArgs e)
        {
            Telas.Pedido.frmConsultarPedido tela = new Telas.Pedido.frmConsultarPedido();
            AbrirTela(tela);
        }

        private void toolStripMenuItem22_Click(object sender, EventArgs e)
        {
            Telas.Entrega.frmCadastrarEntrega tela = new Telas.Entrega.frmCadastrarEntrega();
            AbrirTela(tela);
        }

        private void toolStripMenuItem23_Click(object sender, EventArgs e)
        {
            Telas.Entrega.frmConsultarEntrega tela = new Telas.Entrega.frmConsultarEntrega();
            AbrirTela(tela);
        }

        private void label6_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void panel5_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {

        }

        private void menuStrip3_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LojaFutebol.DB.Entrega;
using LojaFutebol.DB.Pedido;

namespace LojaFutebol.Telas.Entrega
{
    public partial class frmCadastrarEntrega : UserControl
    {
       
        public int PedidoId { get; set; }

        public frmCadastrarEntrega()
        {
            InitializeComponent();
        }

        private void frmCadastrarEntrega_Load(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                EntregaDTO dto = new EntregaDTO();
                dto.rua = textBox3.Text.Trim();
                dto.cep = maskedTextBox1.Text;
                dto.cidade = textBox4.Text.Trim();
                dto.estado = textBox5.Text.Trim();
                dto.PedidoId = this.PedidoId;
                dto.NumeroCasa = Convert.ToInt32(textBox1.Text);

                if (dto.rua == string.Empty)
                {
                    MessageBox.Show("Rua é obrigatória", "SoccerStore");
                }

                if (dto.cep == string.Empty)
                {
                    MessageBox.Show("Cep é obrigatório", "SoccerStore");
                }

                if (dto.cidade == string.Empty)
                {
                    MessageBox.Show("Cidade é obrigatória", "SoccerStore");
                }

                if (dto.estado == string.Empty)
                {
                    MessageBox.Show("Estado é obrigatório", "SoccerStore");
                }
                if(Convert.ToString(dto.NumeroCasa) == string.Empty)
                {
                    MessageBox.Show("Número é obrigatório", "SoccerStore");
                }


                EntregaBusiness business = new EntregaBusiness();
                business.Salvar(dto);

                MessageBox.Show("Entrega salva com sucesso.", "SoccerStore", MessageBoxButtons.OK, MessageBoxIcon.Information);


                this.Hide();
            }
            catch (Exception )
            {
                MessageBox.Show("Todos os campos devem ser preenchidos", "SoccerStore");


            }
          


        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void textBox5_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(char.IsNumber(e.KeyChar) == true)
            {
                e.Handled = true;

            }
            else
            {
                e.Handled = false;
            }

          

        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) == true)
            {
                e.Handled = true;

            }
            else
            {
                e.Handled = false;
            }
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(char.IsNumber(e.KeyChar) == true)
            {
                e.Handled = true;

            }
            else
            {
                e.Handled = false;
            }


        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true)
            {
                e.Handled = true;

            }
            else
            {
                e.Handled = false;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}

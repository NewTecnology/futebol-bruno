﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LojaFutebol.DB.Pedido;
using LojaFutebol.DB;
using LojaFutebol.Telas.Entrega;

namespace LojaFutebol.Telas.Pedido
{
    public partial class FrmCadastrarPedido : UserControl
    {
        BindingList<ProdutoDTO> produtosCarrinho = new BindingList<ProdutoDTO>();
        public FrmCadastrarPedido()
        {
            InitializeComponent();
            CarregarCombos();
            ConfigurarGrid();
        }

        void CarregarCombos()
        {
            ProdutoBusiness business = new ProdutoBusiness();
            List<ProdutoDTO> lista = business.Listar();

            cboProduto.ValueMember = nameof(ProdutoDTO.Id);
            cboProduto.DisplayMember = nameof(ProdutoDTO.Nome);
            cboProduto.DataSource = lista;
        }

        void ConfigurarGrid()
        {
            dgvItens.AutoGenerateColumns = false;
            dgvItens.DataSource = produtosCarrinho;
        }
        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void btnEmitir_Click(object sender, EventArgs e)
        {

            try
            {
                PedidoDTO pedido = new PedidoDTO();
                pedido.Formapgto = comboBox1.Text;
                pedido.Data = DateTime.Now;
                pedido.Cliente = textBox1.Text.Trim();
                PedidoConsultarView pedidos = new PedidoConsultarView();
                pedidos.QtdItens = Convert.ToInt32(numericUpDown1.Value);

                if (pedido.Cliente == string.Empty)
                {
                    MessageBox.Show("Nome do Cliente é obrigatório", "SoccerStore");
                }
                    
                if(pedidos.QtdItens == 0)
                {
                    MessageBox.Show("A Quantidade deve ser maior que  0", "SoccerStore");
                }

                if (pedido.Formapgto == string.Empty)
                {
                    MessageBox.Show("A Forma de pagamento é obrigatória", "SoccerStore");

                }

                PedidoBusiness business = new PedidoBusiness();
                int pedidoId = business.Salvar(pedido, produtosCarrinho.ToList(),pedidos);

                MessageBox.Show("Pedido Salvo com sucesso", "Soccer", MessageBoxButtons.OK, MessageBoxIcon.Information);


                frmCadastrarEntrega tela = new frmCadastrarEntrega();
                tela.PedidoId = pedidoId;

                FrmInicio.Atual.AbrirTela(tela);

                this.Hide();
            }
            catch (Exception)
            {

                MessageBox.Show("Todos os campos devem ser preenchidos", "SoccerStore");
            }
        
        }

        private void FrmCadastrarPedido_Load(object sender, EventArgs e)
        {

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            ProdutoDTO dto = cboProduto.SelectedItem as ProdutoDTO;

            int qtd = Convert.ToInt32(numericUpDown1.Value);


            for (int i = 0; i < qtd; i++)
            {
                produtosCarrinho.Add(dto);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ProdutoDTO dto = cboProduto.SelectedItem as ProdutoDTO;

            int qtd = Convert.ToInt32(numericUpDown1.Value);

            for (int i = 0; i < qtd; i++)
            {
                produtosCarrinho.Remove(dto);
            }
        }

        private void dgvItens_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void txtQuantidade_TextChanged(object sender, EventArgs e)
        {



        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) == true)
            {
                e.Handled = true;

            }
            else
            {
                e.Handled = false;
            }
        }
    }
}

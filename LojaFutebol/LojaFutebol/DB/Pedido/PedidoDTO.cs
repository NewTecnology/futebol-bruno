﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LojaFutebol.DB.Pedido
{
    class PedidoDTO
    {
        public int Id { get; set; }
        public string Formapgto { get; set; }
        public DateTime Data { get; set; }
        public string Cliente { get; set; }

    }
}

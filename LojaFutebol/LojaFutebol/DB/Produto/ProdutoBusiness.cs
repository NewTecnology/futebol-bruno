﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LojaFutebol.DB
{
    class ProdutoBusiness
    {
        public int Salvar(ProdutoDTO dto)
        {
            if (dto.Nome == string.Empty)
              throw new ArgumentException("Nome do produto é obrigatório");

            if (dto.Preco == 0)
              throw new ArgumentException("Preço do produto é obrigatório");

            ProdutoDataBase db = new ProdutoDataBase();
            return db.Salvar(dto);

        }

        public List<ProdutoDTO> Consultar(string produto)
        {
            ProdutoDataBase db = new ProdutoDataBase();
            return db.Consultar(produto);
        }

        public List<ProdutoDTO> Listar()
        {
            ProdutoDataBase db = new ProdutoDataBase();
            return db.Listar();
        }

        public void Remover(int id)
        {
            ProdutoDataBase db = new ProdutoDataBase();
            db.Remover(id);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LojaFutebol.DB.Entrega
{
    class EntregaConsultarView
    {
        public int Id { get; set; }
        public string Cliente { get; set; }
        public string Rua { get; set; }
        public string Cep  { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }
        public int NumeroCasa { get; set; }
      

    }
}

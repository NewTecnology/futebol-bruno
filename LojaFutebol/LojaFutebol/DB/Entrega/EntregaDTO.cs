﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LojaFutebol.DB.Entrega
{
    class EntregaDTO
    {
       public int ID { get; set; }
       public string rua { get; set; }
	   public string cep { get; set; }
	   public string cidade { get; set; } 
	   public string estado { get; set; }
       public int NumeroCasa { get; set; }
       public int PedidoId { get; set; }
    }
}

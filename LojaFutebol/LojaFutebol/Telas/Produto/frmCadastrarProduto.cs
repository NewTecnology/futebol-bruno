﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LojaFutebol.DB;

namespace LojaFutebol.Telas.Produto
{
    public partial class frmCadastrarProduto : UserControl
    {
        public frmCadastrarProduto()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                ProdutoDTO dto = new ProdutoDTO();
                dto.Nome = textBox1.Text.Trim();
                dto.Preco = Convert.ToDecimal(textBox2.Text.Trim());

                if(dto.Nome == string.Empty)
                {
                    MessageBox.Show("Nome do Produto é obrigatório", "SoccerStore");

                }

                if (dto.Preco == 0)
                {
                    MessageBox.Show("Preço é obrigatório", "SoccerStore");

                }


                ProdutoBusiness business = new ProdutoBusiness();
                business.Salvar(dto);

                MessageBox.Show("Produto Salvo com Sucesso", "SoccerStore", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.Hide();

            }
            catch (Exception)
            {

                MessageBox.Show("Todos os campos devem ser preenchidos", "SoccerStore");
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true)
            {
                e.Handled = true;

            }
            else
            {
                e.Handled = false;
            }

           


        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) == true)
            {
                e.Handled = true;

            }
            else
            {
                e.Handled = false;
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LojaFutebol.DB.Pedido
{
    class PedidoBusiness
    {
        public int Salvar(PedidoDTO pedido, List<ProdutoDTO> produtos,PedidoConsultarView pedidos)
        {
            if (pedido.Cliente == string.Empty)
                throw new ArgumentException("Nome do cliente é obrigatório");

            if (pedido.Formapgto == string.Empty)
                throw new ArgumentException("Forma de pagamento é obrigatória");

            if (pedidos.QtdItens == 0) 
            throw new ArgumentException("Quantidade é obrigatória");

            PedidoDataBase pedidoDatabase = new PedidoDataBase();
            int idPedido = pedidoDatabase.Salvar(pedido);

            PedidoItemBusiness itemBusiness = new PedidoItemBusiness();
            foreach (ProdutoDTO item in produtos)
            {
                PedidoItemDTO itemDto = new PedidoItemDTO();
                itemDto.IdPedido = idPedido;
                itemDto.IdProduto = item.Id;

                itemBusiness.Salvar(itemDto);
            }

            return idPedido;
        }

        public List<PedidoConsultarView> Consultar(string cliente)
        {
            PedidoDataBase pedidoDatabase = new PedidoDataBase();
            return pedidoDatabase.Consultar(cliente);
        }



    }
}

﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LojaFutebol.DB.Entrega
{
    class EntregaDataBase
    {
        public int Salvar(EntregaDTO etg)
        {
            string script = @"INSERT INTO tb_entrega (id_pedido, ds_rua,ds_cep,ds_cidade,ds_estado,nr_casa) VALUES (@id_pedido, @ds_rua,@ds_cep,@ds_cidade,@ds_estado,@nr_casa)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_rua", etg.rua));
            parms.Add(new MySqlParameter("ds_cep", etg.cep));
            parms.Add(new MySqlParameter("ds_cidade", etg.cidade));
            parms.Add(new MySqlParameter("ds_estado", etg.estado));
            parms.Add(new MySqlParameter("id_pedido", etg.PedidoId));
            parms.Add(new MySqlParameter("nr_casa", etg.NumeroCasa));


            DataBase db = new DataBase();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<EntregaConsultarView> Consultar (string cliente)
        {
            string script = @"SELECT * FROM vw_entrega_consultar where nm_cliente like @nm_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", cliente + "%"));

            DataBase db = new DataBase();

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<EntregaConsultarView> lista = new List<EntregaConsultarView>();
            while (reader.Read())
            {
                EntregaConsultarView dto = new EntregaConsultarView();

                dto.Id = reader.GetInt32("id_entrega");
                dto.Rua = reader.GetString("ds_rua");
                dto.Cidade = reader.GetString("ds_cidade");
                dto.Estado = reader.GetString("ds_estado");
                dto.Cep = reader.GetString("ds_cep");
                dto.Cliente = reader.GetString("nm_cliente");
                dto.NumeroCasa = reader.GetInt32("nr_casa");
              

                lista.Add(dto);

            }
            reader.Close();

            return lista;

        }

    }   
        
}

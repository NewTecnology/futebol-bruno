﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LojaFutebol.DB.Funcionario
{
    class FuncionarioDatabase
    {
         public int Salvar(FuncionarioDTO Dto)
        {
            string script = @"INSERT Into tb_funcionario (nm_funcionario,ds_rg,ds_cpf,ds_telefone,ds_login,ds_senha)
                              VALUES(@nm_funcionario,@ds_rg,@ds_cpf,@ds_telefone,@ds_login,@ds_senha)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario", Dto.Nome));
            parms.Add(new MySqlParameter("ds_rg", Dto.RG));
            parms.Add(new MySqlParameter("ds_cpf", Dto.CPF));
            parms.Add(new MySqlParameter("ds_telefone", Dto.Telefone));
            parms.Add(new MySqlParameter("ds_login", Dto.Login));
            parms.Add(new MySqlParameter("ds_senha", Dto.Senha));

            DataBase db = new DataBase();
            return db.ExecuteInsertScriptWithPk(script, parms);
            

        }


        public bool Logar(string login, string senha)

       {
            string script = @"SELECT * FROM tb_funcionario WHERE ds_login = @ds_login AND ds_senha = @ds_senha";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_login", login));
            parms.Add(new MySqlParameter("ds_senha", senha));

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            bool logou = false;

            if (reader.Read())
            {
                logou = true;
            }
            else
            {
                logou = false;
            }
            reader.Close();

            return logou;
        }



    }
}

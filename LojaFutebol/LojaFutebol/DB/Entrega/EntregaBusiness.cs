﻿using LojaFutebol.DB.Pedido;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LojaFutebol.DB.Entrega
{
    class EntregaBusiness
    {
        public int Salvar(EntregaDTO dto)
        {
            if (dto.rua == string.Empty)
               throw new ArgumentException("Rua é obrigatória");

            if (dto.cep == string.Empty)
                throw new ArgumentException("Cep é obrigatório");

            if (dto.cidade == string.Empty)
                throw new ArgumentException("Cidade é obrigatória");

            if (dto.estado == string.Empty)
                throw new ArgumentException("Estado é obrigatório");

            if (Convert.ToString(dto.NumeroCasa) == string.Empty)
                throw new ArgumentException("Número é obrigatório");

            EntregaDataBase db = new EntregaDataBase();
            return db.Salvar(dto);

          

        }
            

        public List<EntregaConsultarView> Consultar(string cliente)
        {

            EntregaDataBase db = new EntregaDataBase();
            return db.Consultar(cliente);


        }

    }
}

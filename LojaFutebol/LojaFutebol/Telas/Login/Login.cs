﻿using LojaFutebol.DB.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LojaFutebol
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void panelConteudo_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            FuncionarioBusiness business = new FuncionarioBusiness();
            bool logou = business.Logar(txtLogin.Text, txtSenha.Text);

            if (logou == true)
            {
                FrmInicio inicio = new FrmInicio();
                inicio.ShowDialog();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Credenciais inválidas.", "Soccer Store", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }


        }

        private void button1_Click(object sender, EventArgs e)
        {
            CadastrarLogin tela = new CadastrarLogin();
            tela.ShowDialog();
            this.Hide();
        }

        private void label4_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }
    }
}

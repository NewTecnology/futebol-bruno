﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LojaFutebol.DB.Funcionario
{
    class FuncionarioBusiness
    {
        public int Salvar(FuncionarioDTO dto)
        {
            if (dto.Nome == string.Empty)
                throw new ArgumentException("Nome é obrigatório");

            if (dto.RG == string.Empty)
                throw new ArgumentException("RG é obrigatório");

            if (dto.CPF == string.Empty)
                throw new ArgumentException("CPF é obrigatório");

            if (dto.Login == string.Empty)
                throw new ArgumentException("Login é obrigatório");

            if (dto.Senha == string.Empty)
                throw new ArgumentException("Senha é obrigatória");


            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.Salvar(dto);

        }

        public bool Logar(string login, string senha)
        {
            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.Logar(login, senha);
        }

    }
}

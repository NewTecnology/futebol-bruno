﻿using LojaFutebol.DB.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LojaFutebol
{
    public partial class CadastrarLogin : Form
    {
        public CadastrarLogin()
        {
            InitializeComponent();
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                FuncionarioDTO dto = new FuncionarioDTO();
                dto.Nome = textBox3.Text;
                dto.RG = maskedTextBox1.Text;
                dto.CPF = maskedTextBox2.Text;
                dto.Telefone = maskedTextBox3.Text;
                dto.Login = textBox1.Text;
                dto.Senha = textBox2.Text;

                FuncionarioBusiness business = new FuncionarioBusiness();
                business.Salvar(dto);


                MessageBox.Show("Cadastro efetuado com sucesso", "SoccerStore", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Login tela = new Login();
                tela.ShowDialog();
                this.Hide();
            }
            catch (Exception)
            {

                MessageBox.Show("Todos os campos devem ser preenchidos", "SoccerStore");
            }
          
        }

        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void panelConteudo_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label7_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}

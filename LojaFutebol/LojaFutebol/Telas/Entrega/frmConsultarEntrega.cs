﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LojaFutebol.DB.Entrega;

namespace LojaFutebol.Telas.Entrega
{
    public partial class frmConsultarEntrega : UserControl
    {
        public frmConsultarEntrega()
        {
            InitializeComponent();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            EntregaBusiness business = new EntregaBusiness();

            List<EntregaConsultarView> lista = business.Consultar(textBox1.Text);

            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = lista;

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void frmConsultarEntrega_Load(object sender, EventArgs e)
        {

        }
    }
}
